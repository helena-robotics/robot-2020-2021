# Robot 2020-2021

Robot and controller software for the 2020-2021 academic year.

## About

This repository contains the Java code utilized for the controller and robot.


## Contribute

To contribute to this project, please `push` all contributions to a separate branch. When development is complete, submit a merge request (delete the source branch with the merge). Please ensure that the contributed software is:
+ Properly commented. Otherwise, no one (including you) will know what this is doing a month from now.
+ Consistent. Please ensure spacing, indentation, and style are consistent.
+ Simple. Each function and object should be able to be described with one sentence that doesn't use an `and` or `or`.
+ Exact. Variable names ought to be clear as to their purpose.

GitLab provides a [free Git Cheat Sheet](https://about.gitlab.com/images/press/git-cheat-sheet.pdf) that you may reference during your development efforts. The primary commands of use will likely be `git add .` to add files to your staging area, `git commit` to finalize your changes on your branch, and `git push` to push your changes to your remote branch. Once done with a branch, you'll need to create a new branch via `git branch feature/<name>` and check it out via `git checkout <branch name>`. Changes to your local branch may also be deleted via `git reset [--hard]` if needed.


## Questions?

Please direct all questions to the current adminstrators of the Capital High School Robotics Club, [Kendra Lunday](mailto:klunday@helenaschools.org). Please also include the resident expert, [Quinn Kurokawa](mailto:quinn.kurokawa@mail.umhelena.edu).
